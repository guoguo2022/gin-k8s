package model

import "time"

type Cluster struct {
	Id             int    `gorm:"primary_key"`
	Cluster_Name   string `gorm:"type: varchar(50)"`
	Create_Time    time.Time
	Update_Time    time.Time
	Cluster_Config string `gorm:"type:longText;comment:'配置文件'"`
	Cluster_Type   string `gorm:"type: varchar(20);comment:'集群类型,1、私有云 2、阿里云 3、AWS 4、Azure 5、青云 6、华为云 7、腾讯云'"`
	Status         int    `gorm:"comment:'1:启用,0:禁用'"`
}

func (Cluster) TableName() string {
	return "cluster"
}
