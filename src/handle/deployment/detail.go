package deployment

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/handle/cluster"
	"gitee.com/cmlfxz/gin-k8s/src/handle/hpa"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func deployment_detail(deployment *v1.Deployment) map[string]interface{} {
	meta := deployment.ObjectMeta
	name := meta.Name
	namespace := meta.Namespace
	labels := meta.Labels

	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	// spec := deployment.Spec
	status := deployment.Status
	replicas := deployment.Spec.Replicas
	pods := fmt.Sprintf("%d/%d", status.ReadyReplicas, *replicas)
	strategy := deployment.Spec.Strategy
	spec := deployment.Spec.Template.Spec
	affinity := spec.Affinity
	containers := spec.Containers
	image := containers[0].Image
	node_selector := spec.NodeSelector
	tolerations := spec.Tolerations
	detail := map[string]interface{}{
		"name":          name,
		"namespace":     namespace,
		"pods":          pods,
		"labels":        labels,
		"image":         image,
		"node_selector": node_selector,
		"tolerations":   tolerations,
		"affinity":      affinity,
		"strategy":      strategy,
		"create_time":   create_time,
	}
	return detail
}

func Get_Deployment_Detail(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.AppsV1()

	deployment, err := client.Deployments(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "找不到资源相关信息"})
	} else {

		detail := deployment_detail(deployment)
		h := hpa.Search(namespace, name)
		hpa_detail := hpa.Detail(&h)
		event_list, _ := cluster.Get_Resource_Event_List(namespace, "Deployment", name)

		detail["hpa"] = hpa_detail
		detail["event_list"] = event_list
		c.JSON(200, detail)
	}
}
