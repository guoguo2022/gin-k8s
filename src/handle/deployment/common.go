package deployment

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	v1 "k8s.io/api/apps/v1"
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
)

type InputData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}

type Deploy struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}

func (d *Deploy) get_deploy() *v1.Deployment {
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	deployment, err := clientset.AppsV1().Deployments(d.Namespace).Get(context.TODO(), d.Name, metaV1.GetOptions{})
	if err == nil {
		return deployment
	} else {
		return nil
	}
}
func (d *Deploy) update(body *v1.Deployment) error {
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	client := clientset.AppsV1()
	_, err := client.Deployments(d.Namespace).Update(context.TODO(), body, metaV1.UpdateOptions{})
	fmt.Printf("update: %v\n", err)
	return err
}

func (d *Deploy) patch(patchData []byte) error {
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	client := clientset.AppsV1()
	_, err := client.Deployments(d.Namespace).Patch(context.TODO(), d.Name, types.StrategicMergePatchType, patchData, metaV1.PatchOptions{})
	fmt.Printf("patch: %v\n", err)
	return err
}

func (d *Deploy) update_replicas(replicas int32) bool {
	deployment := d.get_deploy()
	deployment.Spec.Replicas = &replicas
	if err := d.update(deployment); err != nil {
		fmt.Println(err)
		return false
	} else {
		return true
	}
}

func (d *Deploy) delete_node_affinity() error {
	deployment := d.get_deploy()
	affinity := deployment.Spec.Template.Spec.Affinity
	if affinity == nil {
		return errors.New("还没设置亲和性")
	}
	node_affinity := affinity.NodeAffinity
	if node_affinity == nil {
		return errors.New("还没设置节点亲和性")
	}
	deployment.Spec.Template.Spec.Affinity.NodeAffinity = nil
	return d.update(deployment)
}

func (d *Deploy) delete_pod_anti_affinity() error {
	deployment := d.get_deploy()
	affinity := deployment.Spec.Template.Spec.Affinity
	if affinity == nil {
		return errors.New("还没设置亲和性")
	}
	pod_anti_affinity := affinity.PodAntiAffinity
	if pod_anti_affinity == nil {
		return errors.New("还没设置互斥调度")
	}
	deployment.Spec.Template.Spec.Affinity.PodAntiAffinity = nil
	return d.update(deployment)
}

type Node_Affinity_Data struct {
	Type      string `json:"type"`
	MatchType string `json:"match_type"`
	Key       string `json:"key"`
	Value     string `json:"value"`
	Operator  string `json:"operator"`
	Weight    int32  `json:"weight"`
}

func in(target string, str_array []string) bool {
	for _, element := range str_array {
		if target == element {
			return true
		}
	}
	return false
}

func (d *Deploy) add_node_affinity(data Node_Affinity_Data) error {
	if in(data.Type, []string{"required", "preferred"}) == false {
		return errors.New("亲和类型不支持")
	}
	if in(data.MatchType, []string{"matchExpressions", "matchFields"}) == false {
		return errors.New("匹配类型不支持")
	}
	var values []string
	if in(data.Operator, []string{"Exists", "DoesNotExist"}) == false {
		values = strings.Split(data.Value, ",")
	}
	var node_selector coreV1.NodeSelectorTerm
	var node_affinity coreV1.NodeAffinity
	if data.MatchType == "matchExpressions" {
		match_expressions := make([]coreV1.NodeSelectorRequirement, 0)
		expression := coreV1.NodeSelectorRequirement{
			Key:      data.Key,
			Operator: coreV1.NodeSelectorOperator(data.Operator),
			Values:   values,
		}
		match_expressions = append(match_expressions, expression)
		node_selector = coreV1.NodeSelectorTerm{
			MatchExpressions: match_expressions,
		}
		fmt.Printf("expression----node_selector:%v\n", node_selector)
	} else {
		match_fields := make([]coreV1.NodeSelectorRequirement, 0)
		field := coreV1.NodeSelectorRequirement{
			Key:      data.Key,
			Operator: coreV1.NodeSelectorOperator(data.Operator),
			Values:   values,
		}
		match_fields = append(match_fields, field)

		node_selector = coreV1.NodeSelectorTerm{
			MatchFields: match_fields,
		}
		fmt.Printf("fields----node_selector:%v\n", node_selector)
	}

	if data.Type == "preferred" {
		weight := data.Weight
		term := coreV1.PreferredSchedulingTerm{
			Weight:     weight,
			Preference: node_selector,
		}
		preferred_term := make([]coreV1.PreferredSchedulingTerm, 0)
		preferred_term = append(preferred_term, term)
		node_affinity = coreV1.NodeAffinity{
			PreferredDuringSchedulingIgnoredDuringExecution: preferred_term,
		}
		fmt.Printf("preferred----node_affinity:%v\n", node_affinity)
	} else {
		node_selector_terms := make([]coreV1.NodeSelectorTerm, 0)
		node_selector_terms = append(node_selector_terms, node_selector)
		node_affinity = coreV1.NodeAffinity{
			RequiredDuringSchedulingIgnoredDuringExecution: &coreV1.NodeSelector{
				NodeSelectorTerms: node_selector_terms,
			},
		}
		fmt.Printf("require----node_affinity:%v\n", node_affinity)
	}

	deployment := d.get_deploy()
	affinity := deployment.Spec.Template.Spec.Affinity
	fmt.Printf("affinity: %v\n", affinity)
	if affinity == nil {
		affinity = &coreV1.Affinity{NodeAffinity: &node_affinity}
		deployment.Spec.Template.Spec.Affinity = affinity
		return d.update(deployment)
	} else {
		affinity.NodeAffinity = &node_affinity
		// patchData, _ := json.Marshal(affinity)
		// return d.patch(patchData)
		deployment.Spec.Template.Spec.Affinity = affinity
		return d.update(deployment)
	}
}
