package deployment

import (
	"errors"
	"fmt"
	"strings"

	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Pod_Anti_Affinity_Data struct {
	Type        string `json:"type"`
	MatchType   string `json:"match_type"`
	Key         string `json:"key"`
	Value       string `json:"value"`
	Operator    string `json:"operator"`
	Weight      int32  `json:"weight"`
	TopologyKey string `json:"topologyKey"`
}

func (d *Deploy) add_pod_anti_affinity(data Pod_Anti_Affinity_Data) error {
	if in(data.Type, []string{"required", "preferred"}) == false {
		return errors.New("亲和类型不支持")
	}
	if in(data.MatchType, []string{"matchExpressions", "matchLabels"}) == false {
		return errors.New("匹配类型不支持")
	}
	var label_selector metaV1.LabelSelector
	var pod_anti_affinity coreV1.PodAntiAffinity
	if data.MatchType == "matchExpressions" {
		var values []string
		if in(data.Operator, []string{"Exists", "DoesNotExist"}) == false {
			values = strings.Split(data.Value, ",")
		}
		label_selector = metaV1.LabelSelector{
			MatchExpressions: []metaV1.LabelSelectorRequirement{
				{
					Key:      data.Key,
					Operator: metaV1.LabelSelectorOperator(data.Operator),
					Values:   values,
				},
			},
		}
		fmt.Printf("expression----label_selector:%v\n", label_selector)
	} else {
		//不支持value为数组
		if len(strings.Split(data.Value, ",")) > 1 {
			return errors.New("标签匹配模式下不支持values为数组")
		}
		match_labels := map[string]string{
			data.Key: data.Value,
		}
		label_selector = metaV1.LabelSelector{
			MatchLabels: match_labels,
		}
	}
	if data.Type == "required" {
		pod_anti_affinity = coreV1.PodAntiAffinity{
			RequiredDuringSchedulingIgnoredDuringExecution: []coreV1.PodAffinityTerm{
				{
					LabelSelector: &label_selector,
					TopologyKey:   data.TopologyKey,
				},
			},
		}
		fmt.Printf("require----pod_anti_affinity:%v\n", pod_anti_affinity)
	} else {
		pod_anti_affinity = coreV1.PodAntiAffinity{
			PreferredDuringSchedulingIgnoredDuringExecution: []coreV1.WeightedPodAffinityTerm{
				{
					Weight: data.Weight,
					PodAffinityTerm: coreV1.PodAffinityTerm{
						LabelSelector: &label_selector,
						TopologyKey:   data.TopologyKey,
					},
				},
			},
		}
		fmt.Printf("preferred----pod_anti_affinity:%v\n", pod_anti_affinity)
	}
	deployment := d.get_deploy()
	affinity := deployment.Spec.Template.Spec.Affinity
	fmt.Printf("affinity: %v\n", affinity)
	if affinity == nil {
		affinity = &coreV1.Affinity{PodAntiAffinity: &pod_anti_affinity}
		deployment.Spec.Template.Spec.Affinity = affinity
		return d.update(deployment)
	} else {
		affinity.PodAntiAffinity = &pod_anti_affinity
		deployment.Spec.Template.Spec.Affinity = affinity
		return d.update(deployment)
	}
}
