package deployment

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_Deployment_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	var deployments *v1.DeploymentList

	if namespace == "all" || namespace == "" {
		deployments, _ = clientset.AppsV1().Deployments("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		deployments, _ = clientset.AppsV1().Deployments(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, deployment := range deployments.Items {
		detail := deployment_detail(&deployment)
		list = append(list, detail)

	}
	fmt.Println(len(list))
	c.JSON(200, list)
}

func Get_Problem_Deployment(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	var deployments *v1.DeploymentList

	if namespace == "all" || namespace == "" {
		deployments, _ = clientset.AppsV1().Deployments("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		deployments, _ = clientset.AppsV1().Deployments(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	problem_deployments := make([]*v1.Deployment, 0)
	for _, item := range deployments.Items {
		replicas := item.Spec.Replicas
		ready_replicas := item.Status.ReadyReplicas
		if *replicas != ready_replicas {
			problem_deployments = append(problem_deployments, &item)
		}
	}

	list := make([]map[string]interface{}, 0)
	for _, deployment := range problem_deployments {
		detail := deployment_detail(deployment)
		list = append(list, detail)

	}
	c.JSON(200, list)
}

func Get_Deployment_Toleration(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.AppsV1()
	deployement, err := client.Deployments(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err == nil {
		tolerations := deployement.Spec.Template.Spec.Tolerations
		c.JSON(200, tolerations)
	} else {
		c.JSON(500, gin.H{"fail": "找不到deployment相关信息"})
	}

}

func Get_Deployment_Replicas(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接不上"})
	}
	client := clientset.AppsV1()
	deployement, err := client.Deployments(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err == nil {
		c.JSON(200, gin.H{"fail": "查找deploy出现错误"})
	} else {
		replicas := deployement.Status.Replicas
		c.JSON(500, gin.H{"replicas": replicas})
	}
}

func Delete_Deployment(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.AppsV1()
	if namespace == "all" || namespace == "" {
		c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		return
	}
	err = client.Deployments(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
	if err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败"})
	}

}
