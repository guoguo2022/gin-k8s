package cluster

import (
	"context"
	"errors"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_Event_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var events *v1.EventList
	if namespace == "all" || namespace == "" {
		events, _ = client.Events("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		events, _ = client.Events(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, event := range events.Items {
		meta := event.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		last_seen := event.LastTimestamp.Local().Format("2006-01-02 15:04:05")
		message := event.Message
		reason := event.Reason
		event_type := event.Type
		source := event.Source.Component
		kind := event.InvolvedObject.Kind
		subobject := event.InvolvedObject.Name

		object := fmt.Sprintf("%s/%s", kind, subobject)

		detail := map[string]interface{}{
			"name":      name,
			"namespace": namespace,
			"last_seen": last_seen,
			"message":   message,
			"reason":    reason,
			"type":      event_type,
			"object":    object,
			"source":    source,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Get_Resource_Event_List(namespace string, input_kind string, input_name string) (result []map[string]interface{}, err error) {

	fmt.Printf("get_resource_event_list参数:%s,%s,%s\n", namespace, input_kind, input_name)
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return nil, errors.New("k8s链接不上")
	}
	client := clientset.CoreV1()
	var events *v1.EventList
	if namespace == "all" || namespace == "" {
		return nil, errors.New("必须指定具体命名空间")
	} else {
		events, err = client.Events(namespace).List(context.TODO(), metaV1.ListOptions{})
		if err != nil {
			return nil, err
		}
	}

	list := make([]map[string]interface{}, 0)
	for _, event := range events.Items {
		kind := event.InvolvedObject.Kind
		subobject := event.InvolvedObject.Name
		if kind == input_kind && subobject == input_name {
			source := event.Source.Component
			first_time := event.FirstTimestamp.Local().Format("2006-01-02 15:04:05")
			last_seen := event.LastTimestamp.Local().Format("2006-01-02 15:04:05")
			message := event.Message
			reason := event.Reason

			detail := map[string]interface{}{
				"message":    message,
				"reason":     reason,
				"source":     source,
				"last_seen":  last_seen,
				"first_seen": first_time,
			}
			list = append(list, detail)
		}

	}
	return list, nil

}
