package storage

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_pvc_list = "http://192.168.11.5:8090/k8s/get_pvc_list"
	select_pvc   = "http://192.168.11.5:8090/k8s/select_pvc"
)

func Test_PVC_List(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "all"
	result, err := httpclient.Post(get_pvc_list, data, headers)
	fmt.Println(string(result), err)
}

func Test_PVC_Select(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["name"] = "pvc-pv-1g-rwx-nfs-delete"
	result, err := httpclient.Post(select_pvc, data, headers)
	fmt.Println(string(result), err)
}
