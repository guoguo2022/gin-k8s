package storage

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func pvc_detail(pvc *v1.PersistentVolumeClaim) map[string]interface{} {
	meta := pvc.ObjectMeta
	name := meta.Name
	namespace := meta.Namespace
	create_time := pvc.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	spec := pvc.Spec
	access_modes := spec.AccessModes[0]
	resources := spec.Resources
	capacity := resources.Requests["storage"]
	storage_class_name := spec.StorageClassName
	volume_name := spec.VolumeName

	status := pvc.Status.Phase
	detail := map[string]interface{}{
		"name":               name,
		"namespace":          namespace,
		"pv":                 volume_name,
		"status":             status,
		"capacity":           capacity,
		"resources":          resources,
		"access_modes":       access_modes,
		"storage_class_name": storage_class_name,
		"create_time":        create_time,
	}
	return detail
}

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_PVC_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	client := clientset.CoreV1()
	var pvcs *v1.PersistentVolumeClaimList
	if namespace == "all" || namespace == "" {
		pvcs, _ = client.PersistentVolumeClaims("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		pvcs, _ = client.PersistentVolumeClaims(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, pvc := range pvcs.Items {
		detail := pvc_detail(&pvc)
		list = append(list, detail)
	}
	c.JSON(200, list)
}

type SelectData struct {
	Name string `json:"name"`
}

func Select_PVC(c *gin.Context) {
	var data SelectData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	field_selector := fmt.Sprintf("metadata.name=%s", name)
	pvcs, err := client.PersistentVolumeClaims("").List(context.TODO(), metaV1.ListOptions{FieldSelector: field_selector})
	fmt.Println(err)
	list := make([]map[string]interface{}, 0)
	if err == nil {
		detail := pvc_detail(&pvcs.Items[0])
		list = append(list, detail)
		c.JSON(200, list)
	} else {
		c.JSON(500, gin.H{"fail": "查询不到pvc的数据"})
	}
}

func Delete_PVC(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace

	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	if namespace == "all" || namespace == "" {
		c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
	} else {
		err := client.PersistentVolumeClaims(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
		if err == nil {
			c.JSON(200, gin.H{"ok": "删除成功"})
		} else {
			c.JSON(500, gin.H{"fail": "删除失败"})
		}
	}
}
