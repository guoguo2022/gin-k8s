package pod

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func get_pod_status(pod *v1.Pod) string {
	meta := pod.ObjectMeta
	name := meta.Name
	status := pod.Status

	container_status := status.Phase
	if len(status.ContainerStatuses) > 0 {
		for _, item := range status.ContainerStatuses {
			if strings.Contains(name, item.Name) {
				if item.State.Waiting != nil {
					container_status = v1.PodPhase(item.State.Waiting.Reason)
				} else if item.State.Terminated != nil {
					container_status = v1.PodPhase(item.State.Terminated.Reason)
				} else {
					fmt.Println("nothing to do")
				}
			}
		}
	}
	return string(container_status)
}

// func namespaced_pods(namespace string) ([]v1.Pod, error) {
// 	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
// 	if err != nil {
// 		return nil, errors.New("k8s连接不上")
// 	}
// 	client := clientset.CoreV1()
// 	var pods *v1.PodList
// 	if namespace == "all" || namespace == "" {
// 		pods, _ = client.Pods("").List(context.TODO(), metaV1.ListOptions{})
// 	} else {
// 		pods, _ = client.Pods(namespace).List(context.TODO(), metaV1.ListOptions{})
// 	}
// 	return pods.Items, nil
// }

func namespaced_pods(namespace string) (*[]v1.Pod, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return nil, errors.New("k8s连接不上")
	}
	client := clientset.CoreV1()
	var pods *v1.PodList
	if namespace == "all" || namespace == "" {
		pods, _ = client.Pods("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		pods, _ = client.Pods(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	return &pods.Items, nil
}
func problem_pods(namespace string) ([]*v1.Pod, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return nil, errors.New("k8s连接不上")
	}
	client := clientset.CoreV1()
	var pods *v1.PodList
	if namespace == "all" || namespace == "" {
		pods, _ = client.Pods("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		pods, _ = client.Pods(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	problem_pods := make([]*v1.Pod, 0)
	for _, pod := range pods.Items {
		status := get_pod_status(&pod)
		if status != "Running" {
			problem_pods = append(problem_pods, &pod)
		}
	}
	return problem_pods, nil
}

// func node_pods(node string) ([]v1.Pod, error) {
// 	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
// 	if err != nil {
// 		return nil, errors.New("k8s连接不上")
// 	}
// 	client := clientset.CoreV1()
// 	var pods *v1.PodList
// 	field_selector := fmt.Sprintf("spec.nodeName=%s", node)
// 	pods, _ = client.Pods("").List(context.TODO(), metaV1.ListOptions{FieldSelector: field_selector})
// 	return pods.Items, nil
// }

func node_pods(node string) (*[]v1.Pod, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return nil, errors.New("k8s连接不上")
	}
	client := clientset.CoreV1()
	var pods *v1.PodList
	field_selector := fmt.Sprintf("spec.nodeName=%s", node)
	pods, _ = client.Pods("").List(context.TODO(), metaV1.ListOptions{FieldSelector: field_selector})
	return &pods.Items, nil
}

// 指向slice的指针
func page_pods_v2(podsAddr *[]v1.Pod, pageSize int, page int) map[string]interface{} {
	pods := (*podsAddr)
	from := (page - 1) * pageSize
	end := page * pageSize
	total := len(pods)
	if end > total {
		end = total
	}
	list := make([]map[string]interface{}, 0)
	for _, pod := range pods[from:end] {
		detail := Namespace_Pod_Detail(&pod)
		list = append(list, detail)
	}
	result := map[string]interface{}{
		"pod_list": list,
		"total":    total,
	}
	return result
}

func page_pods(pods []*v1.Pod, pageSize int, page int) map[string]interface{} {
	from := (page - 1) * pageSize
	end := page * pageSize
	total := len(pods)
	if end > total {
		end = total
	}
	list := make([]map[string]interface{}, 0)
	for _, pod := range pods[from:end] {
		detail := Namespace_Pod_Detail(pod)
		list = append(list, detail)
	}
	result := map[string]interface{}{
		"pod_list": list,
		"total":    total,
	}
	return result
}
