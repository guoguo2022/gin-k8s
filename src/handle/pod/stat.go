package pod

import (
	"context"
	"errors"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"gitee.com/cmlfxz/gin-k8s/src/util"
)

//不计算边车的
func Pod_Resource(containers *[]v1.Container) map[string]interface{} {
	c := (*containers)[0]
	var req_c, limit_c int64 = 0, 0
	var req_m, limit_m float64 = 0, 0

	resources := c.Resources

	requests := resources.Requests

	if len(requests) > 0 {
		// fmt.Printf("requests memory valie:%v mill:%v\n", requests.Memory().Value(), requests.Memory().MilliValue())
		req_c = requests.Cpu().MilliValue()
		req_m = util.Handle_Memory(requests.Memory().Value())
	}
	limits := resources.Limits
	if len(limits) > 0 {
		// fmt.Printf("limits memory valie:%v mill:%v\n", limits.Memory().Value(), limits.Memory().MilliValue())
		limit_c = limits.Cpu().MilliValue()
		limit_m = util.Handle_Memory(limits.Memory().Value())
	}
	r := map[string]interface{}{
		"requests": map[string]interface{}{
			"cpu":    req_c,
			"memory": req_m,
		},
		"limits": map[string]interface{}{
			"cpu":    limit_c,
			"memory": limit_m,
		},
	}
	return r
}

type Requests struct {
	Cpu    int64   `json:"cpu"`
	Memory float64 `json:"memory"`
}

type Limits struct {
	Cpu    int64   `json:"cpu"`
	Memory float64 `json:"memory"`
}
type Resources struct {
	Requests Requests `json:"requests"`
	Limits   Limits   `json:"limits"`
}

//计算边车的
func Whole_Pod_Resource(containers *[]v1.Container) Resources {
	cs := (*containers)

	var pod_req_c, pod_limit_c int64 = 0, 0
	var pod_req_m, pod_limit_m float64 = 0, 0

	for _, c := range cs {
		resources := c.Resources

		requests := resources.Requests
		if len(requests) > 0 {
			// fmt.Printf("requests memory valie:%v mill:%v\n", requests.Memory().Value(), requests.Memory().MilliValue())
			req_c := requests.Cpu().MilliValue()
			req_m := util.Handle_Memory(requests.Memory().Value())
			pod_req_c += req_c
			pod_req_m += req_m
		}
		limits := resources.Limits
		if len(limits) > 0 {
			// fmt.Printf("limits memory valie:%v mill:%v\n", limits.Memory().Value(), limits.Memory().MilliValue())
			limit_c := limits.Cpu().MilliValue()
			limit_m := util.Handle_Memory(limits.Memory().Value())
			pod_limit_c += limit_c
			pod_limit_m += limit_m
		}
	}
	r := Resources{
		Requests: Requests{
			Cpu:    pod_req_c,
			Memory: pod_req_m,
		},
		Limits: Limits{
			Cpu:    pod_limit_c,
			Memory: pod_limit_m,
		},
	}
	return r
}

type NamespaceResouces struct {
	Namespace string `json:"namespace"`
	Resources
}

//统计ns下所有pod的cpu,memory: req,limit数据
func Namespace_Resources_Stat(namespace string) (Resources, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return Resources{}, errors.New("k8s链接失败")
	}
	client := clientset.CoreV1()
	pods, err := client.Pods(namespace).List(context.TODO(), metaV1.ListOptions{})
	if err != nil {
		return Resources{}, errors.New("找不到资源")
	}
	r := Total_Pod_Resources(&pods.Items)
	return r, nil
}

// 统计pod list的资源
func Total_Pod_Resources(podsPtr *[]v1.Pod) Resources {
	pods := (*podsPtr)
	var total_req_c, total_limit_c int64 = 0, 0
	var toal_req_m, toal_limit_m float64 = 0, 0
	for _, pod := range pods {
		containers := &pod.Spec.Containers
		pod_resources := Whole_Pod_Resource(containers)
		requests := pod_resources.Requests
		limits := pod_resources.Limits

		req_c := requests.Cpu
		req_m := requests.Memory

		limit_c := limits.Cpu
		limit_m := limits.Memory

		total_req_c += req_c
		toal_req_m += req_m

		total_limit_c += limit_c
		toal_limit_m += limit_m
	}
	r := Resources{
		Requests: Requests{
			Cpu:    total_req_c,
			Memory: toal_req_m,
		},
		Limits: Limits{
			Cpu:    total_limit_c,
			Memory: toal_limit_m,
		},
	}
	return r
}

type NodeResources struct {
	Resources Resources `json:"resources"`
	Pod_Num   int       `json:"pod_num"`
}

//统计node下所有pod的cpu,memory: req,limit,pod数量数据
func Node_Resources_Stat(name string) (NodeResources, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return NodeResources{}, errors.New("k8s链接失败")
	}
	client := clientset.CoreV1()
	field_seletor := fmt.Sprintf("spec.nodeName=%s", name)
	pods, err := client.Pods("").List(context.TODO(), metaV1.ListOptions{FieldSelector: field_seletor})

	if err != nil {
		return NodeResources{}, errors.New("找不到资源")
	}
	pod_num := len(pods.Items)
	r := Total_Pod_Resources(&pods.Items)
	nr := NodeResources{
		Resources: r,
		Pod_Num:   pod_num,
	}
	return nr, nil
}
