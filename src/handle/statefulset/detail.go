package statefulset

import (
	"fmt"

	v1 "k8s.io/api/apps/v1"
	coreV1 "k8s.io/api/core/v1"
)

func create_single_statefulset_object(statefulset *v1.StatefulSet) (map[string]interface{}, error) {
	meta := statefulset.ObjectMeta
	name := meta.Name
	namespace := meta.Namespace
	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")

	labels := meta.Labels
	spec := statefulset.Spec
	selector := spec.Selector
	revision_history_limit := spec.RevisionHistoryLimit
	replicas := spec.Replicas
	// pod template
	template := spec.Template
	template_labels := template.ObjectMeta.Labels

	template_spec := template.Spec
	var node_affinity *coreV1.NodeAffinity
	var pod_affinity *coreV1.PodAffinity
	var pod_anti_affinity *coreV1.PodAntiAffinity
	affinity := template_spec.Affinity
	if affinity != nil {
		node_affinity = affinity.NodeAffinity
		pod_affinity = affinity.PodAffinity
		pod_anti_affinity = affinity.PodAntiAffinity
	}

	containers := template_spec.Containers
	container_list := make([]interface{}, 0)

	for _, container := range containers {
		image := container.Image
		name := container.Name
		mycontainer := map[string]interface{}{
			"name":  name,
			"image": image,
		}
		container_list = append(container_list, mycontainer)
	}
	host_network := template_spec.HostNetwork
	tolerations := template_spec.Tolerations

	status := statefulset.Status
	ready_replicas := status.ReadyReplicas
	updated_replicas := status.UpdatedReplicas
	current_replicas := status.CurrentReplicas
	ready := fmt.Sprintf("%d/%d", ready_replicas, replicas)

	detail := map[string]interface{}{
		"name":                   name,
		"namespace":              namespace,
		"selector":               selector,
		"replicas":               replicas,
		"ready":                  ready,
		"current_replicas":       current_replicas,
		"up-to-date":             updated_replicas,
		"labels":                 labels,
		"host_network":           host_network,
		"tolerations":            tolerations,
		"container_list":         container_list,
		"status":                 status,
		"template_labels":        template_labels,
		"revision_history_limit": revision_history_limit,
		"node_affinity":          node_affinity,
		"pod_affinity":           pod_affinity,
		"pod_anti_affinity":      pod_anti_affinity,

		"create_time": create_time,
	}
	return detail, nil
}
