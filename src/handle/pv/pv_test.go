package pv

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_pv_list = "http://192.168.11.5:8090/k8s/get_pv_list"
	select_pv   = "http://192.168.11.5:8090/k8s/select_pv"
	create_pv   = "http://192.168.11.5:8090/k8s/create_pv"
)

// func Test_PV_List(t *testing.T) {
// 	headers := make(map[string]string)
// 	headers["cluster_name"] = "k3s_112"
// 	data := make(map[string]interface{})
// 	// data["namespace"] = "all"
// 	result, err := httpclient.Post(get_pv_list, data, headers)
// 	fmt.Println(string(result), err)
// }

// func Test_PV_Select(t *testing.T) {
// 	headers := make(map[string]string)
// 	headers["cluster_name"] = "k3s_112"
// 	data := make(map[string]interface{})
// 	data["name"] = "pv-1g-rwo-nfs-retain"
// 	result, err := httpclient.Post(select_pv, data, headers)
// 	fmt.Println(string(result), err)
// }

func Test_Create_Pv(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := map[string]interface{}{
		"name":             "pv-rwx-nfs-3388",
		"capacity":         "1Gi",
		"accessModes":      "ReadWriteMany",
		"reclaimPolicy":    "Retain",
		"storageClassName": "",
		"source":           "nfs",
		"nfs": map[string]interface{}{
			"path":     "/NAS",
			"server":   "192.168.11.31",
			"readonly": false,
		},
	}

	result, err := httpclient.Post(create_pv, data, headers)
	fmt.Println(string(result), err)
}
