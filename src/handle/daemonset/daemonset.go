package daemonset

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"

	// v1 "k8s.io/api/core/v1"
	v1 "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_DaemonSet_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.AppsV1()
	var daemonsets *v1.DaemonSetList
	if namespace == "all" || namespace == "" {
		daemonsets, _ = client.DaemonSets("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		daemonsets, _ = client.DaemonSets(namespace).List(context.TODO(), metaV1.ListOptions{})
	}

	list := make([]map[string]interface{}, 0)
	for _, daemonset := range daemonsets.Items {
		meta := daemonset.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace

		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")

		labels := meta.Labels
		template := daemonset.Spec.Template
		template_spec := template.Spec

		affinity := template_spec.Affinity
		containers := template_spec.Containers
		container_list := make([]interface{}, 0)

		for _, container := range containers {
			image := container.Image
			// volume_mounts := container.VolumeMounts
			// env := container.Env
			mycontainer := map[string]interface{}{
				"image": image,
			}
			container_list = append(container_list, mycontainer)
		}
		host_network := template_spec.HostNetwork
		tolerations := template_spec.Tolerations

		status := daemonset.Status
		mystatus := fmt.Sprintf("%d/%d", status.NumberReady, status.DesiredNumberScheduled)
		detail := map[string]interface{}{
			"name":         name,
			"namespace":    namespace,
			"labels":       labels,
			"affinity":     affinity,
			"tolerations":  tolerations,
			"containers":   container_list,
			"host_network": host_network,
			"status":       mystatus,
			"create_time":  create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Delete_DaemonSet(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
	} else {
		client := clientset.AppsV1()
		if namespace == "all" || namespace == "" {
			c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		} else {
			err := client.DaemonSets(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
			if err == nil {
				c.JSON(200, gin.H{"ok": "删除成功"})
			} else {
				c.JSON(500, gin.H{"fail": "删除失败"})
			}
		}
	}
}
