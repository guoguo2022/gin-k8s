package hpa

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	v1 "k8s.io/api/autoscaling/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func Search(namespace string, deploy_name string) v1.HorizontalPodAutoscaler {
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	client := clientset.AutoscalingV1()
	hpas, _ := client.HorizontalPodAutoscalers(namespace).List(context.TODO(), metaV1.ListOptions{})
	var hpa v1.HorizontalPodAutoscaler

	for _, item := range hpas.Items {
		ref := item.Spec.ScaleTargetRef
		kind := ref.Kind
		name := ref.Name
		if kind == "Deployment" && name == deploy_name {
			hpa = item
			break
		}
	}
	return hpa
}

func Detail(hpa *v1.HorizontalPodAutoscaler) map[string]interface{} {
	meta := hpa.ObjectMeta
	name := meta.Name
	namespace := meta.Namespace
	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	spec := hpa.Spec
	maxReplicas := spec.MaxReplicas
	minReplicas := spec.MinReplicas
	scaleTargetRef := spec.ScaleTargetRef
	targetCPUUtilizationPercentage := spec.TargetCPUUtilizationPercentage

	status := hpa.Status
	currentCPUUtilizationPercentage := status.CurrentCPUUtilizationPercentage
	current_replicas := status.CurrentReplicas

	detail := map[string]interface{}{
		"name":                            name,
		"namespace":                       namespace,
		"currentReplicas":                 current_replicas,
		"minReplicas":                     minReplicas,
		"maxReplicas":                     maxReplicas,
		"scaleTargetRef":                  scaleTargetRef,
		"targetCPUUtilizationPercentage":  targetCPUUtilizationPercentage,
		"currentCPUUtilizationPercentage": currentCPUUtilizationPercentage,
		"create_time":                     create_time,
	}
	return detail

}
