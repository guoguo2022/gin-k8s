package service

type ListData struct {
	Namespace string `json:"namespace"`
}

type InputData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}
