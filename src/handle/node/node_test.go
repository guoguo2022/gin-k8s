package node

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_node_list = "http://192.168.11.5:8090/k8s/get_node_list"
)

func Test_Auth(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	result, err := httpclient.Post(get_node_list, data, headers)
	fmt.Println(string(result), err)
}
