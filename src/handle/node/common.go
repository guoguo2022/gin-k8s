package node

import (
	"context"
	"errors"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type MyNode struct {
	node *v1.Node
	name string
}

func (n *MyNode) Update() (bool, error) {
	clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	client := clientset.CoreV1()
	_, err := client.Nodes().Update(context.TODO(), n.node, metaV1.UpdateOptions{})
	if err == nil {
		return true, nil
	} else {
		return false, err
	}
}

func (n *MyNode) Add_Taint(t v1.Taint) (bool, error) {
	taints := n.node.Spec.Taints
	taints = append(taints, t)
	n.node.Spec.Taints = taints
	return n.Update()
}

func (n *MyNode) Delete_Taint(t v1.Taint) (bool, error) {
	taints := n.node.Spec.Taints
	var i int = -1
	for index, taint := range taints {
		if t.Key == taint.Key && t.Value == taint.Value && t.Effect == taint.Effect {
			i = index
			break
		}
	}
	if i == -1 {
		return false, errors.New("无此taint")
	}
	taints = append(taints[:i], taints[i+1:]...)
	n.node.Spec.Taints = taints
	return n.Update()
}

func (n *MyNode) Add_Labels(label map[string]string) (bool, error) {
	labels := n.node.ObjectMeta.Labels
	for key, value := range label {
		labels[key] = value
	}
	n.node.ObjectMeta.Labels = labels
	return n.Update()
}

func (n *MyNode) Delete_Labels(label map[string]string) (bool, error) {
	labels := n.node.ObjectMeta.Labels
	for key, _ := range label {
		delete(labels, key)
	}
	n.node.ObjectMeta.Labels = labels
	return n.Update()
}
