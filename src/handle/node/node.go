package node

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"gitee.com/cmlfxz/gin-k8s/src/util"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func node_detail(node v1.Node) (map[string]interface{}, error) {
	meta := node.ObjectMeta
	name := meta.Name
	labels := meta.Labels
	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	role := labels["kubernetes.io/role"]
	spec := node.Spec
	pod_cidr := spec.PodCIDR
	taints := spec.Taints
	status := node.Status
	var ready v1.ConditionStatus
	conditions := status.Conditions
	for _, item := range conditions {
		if item.Type == "Ready" {
			ready = item.Status
		}
	}

	node_info := status.NodeInfo

	capacity := status.Capacity

	cpu_total := capacity.Cpu().Value()
	// fmt.Println("memory ********: %v :%v", capacity.Memory().Value(), capacity.Memory().MilliValue())
	memory_total := util.Handle_Memory(capacity.Memory().Value())
	pod_total := capacity.Pods().Value()
	disk_space := util.Handle_Disk(capacity.StorageEphemeral().Value())

	detail := map[string]interface{}{
		"name":         name,
		"role":         role,
		"labels":       labels,
		"ready":        ready,
		"node_info":    node_info,
		"taints":       taints,
		"pod_cidr":     pod_cidr,
		"cpu_total":    cpu_total,
		"memory_total": memory_total,
		"pod_total":    pod_total,
		"disk_space":   disk_space,
		"create_time":  create_time,
	}
	return detail, nil
}
func Get_Node_List(c *gin.Context) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	var nodes *v1.NodeList
	nodes, _ = client.Nodes().List(context.TODO(), metaV1.ListOptions{})

	list := make([]map[string]interface{}, 0)
	for _, node := range nodes.Items {
		detail, _ := node_detail(node)
		list = append(list, detail)
	}
	c.JSON(200, list)
}
func Get_Node_Name_List(c *gin.Context) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	var nodes *v1.NodeList
	nodes, _ = client.Nodes().List(context.TODO(), metaV1.ListOptions{})
	list := make([]string, 0)
	for _, node := range nodes.Items {
		name := node.ObjectMeta.Name
		list = append(list, name)
	}
	c.JSON(200, list)
}

type UpdateData struct {
	Name   string            `json:"node_name"`
	Action string            `json:"action"`
	Taint  v1.Taint          `json:"taint"`
	Labels map[string]string `json:"labels"`
}

func Update(c *gin.Context) {
	var data UpdateData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
	} else {
		fmt.Println(data)
		clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
		client := clientset.CoreV1()
		node, err := client.Nodes().Get(context.TODO(), data.Name, metaV1.GetOptions{})
		if err != nil {
			fmt.Println(err)
			c.JSON(500, gin.H{"fail": "找不到node"})
		} else {
			mynode := MyNode{node: node, name: data.Name}
			action := data.Action
			var ok bool
			var err error
			if action == "add_taint" {
				taint := data.Taint
				ok, err = mynode.Add_Taint(taint)
			} else if action == "delete_taint" {
				taint := data.Taint
				ok, err = mynode.Delete_Taint(taint)
			} else if action == "add_labels" {
				labels := data.Labels
				ok, err = mynode.Add_Labels(labels)
			} else if action == "delete_labels" {
				labels := data.Labels
				ok, err = mynode.Delete_Labels(labels)
			} else {
				c.JSON(500, gin.H{"fail": "不支持的操作"})
			}
			if ok {
				c.JSON(200, gin.H{"ok": "操作成功"})
			} else {
				fmt.Println(err)
				c.JSON(500, gin.H{"fail": "操作失败"})
			}
		}
	}
}
