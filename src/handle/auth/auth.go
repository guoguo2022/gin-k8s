package auth

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_Service_Account_List(c *gin.Context) {

	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	var sas *v1.ServiceAccountList
	if namespace == "all" || namespace == "" {
		sas, _ = clientset.CoreV1().ServiceAccounts("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		sas, _ = clientset.CoreV1().ServiceAccounts(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, sa := range sas.Items {
		meta := sa.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace

		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		secrets := sa.Secrets
		secret_list := make([]map[string]string, 0)
		for _, secret := range secrets {
			secret_name := secret.Name
			mysecret := map[string]string{
				"name": secret_name,
			}
			secret_list = append(secret_list, mysecret)
		}

		detail := map[string]interface{}{
			"name":        name,
			"namespace":   namespace,
			"secret_list": secret_list,
			"create_time": create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

type InputData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}

func Delete_Service_Account(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()

	deletePolicy := metaV1.DeletePropagationBackground
	err = client.ServiceAccounts(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{
		PropagationPolicy: &deletePolicy,
	})
	if err != nil {
		c.JSON(500, gin.H{"fail": "删除失败"})
	} else {
		c.JSON(200, gin.H{"ok": "删除成功"})
	}

}
