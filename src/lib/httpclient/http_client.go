package httpclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const componentIDGINHttpServer = 5006

func SimplePost(url string) (result []byte, err error) {
	client := &http.Client{}
	req, err := http.NewRequest("POST", url, nil)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("请求不成功:", err.Error())
		return nil, err
	}
	// 前面必须判断err ,不然这下面关闭会报空指针
	defer resp.Body.Close()
	if err != nil {
		fmt.Println("请求出错")
		return nil, err
	}
	return ioutil.ReadAll(resp.Body)

}

func Post(url string, data map[string]interface{}, headers map[string]string) (result []byte, err error) {
	client := &http.Client{}

	// 处理数据
	bytesData, err := json.Marshal(data)
	if err != nil {
		// fmt.Println(err.Error())
		return nil, err
	}
	body := bytes.NewReader(bytesData)
	// 处理header
	req, err := http.NewRequest("POST", url, body)
	// 添加header
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("请求不成功:", err.Error())
		return nil, err
	}
	// 前面必须判断err ,不然这下面关闭会报空指针
	defer resp.Body.Close()
	if err != nil {
		fmt.Println("请求出错")
		return nil, err
	}
	return ioutil.ReadAll(resp.Body)

}
