package gredis

import (
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/conf"
	"github.com/go-redis/redis"
)

//var client redis.Client
func NewRedisClient(db int) *redis.Client {
	addr := conf.Setting.RedisConfig.Addr
	password := conf.Setting.RedisConfig.Password
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	})
	pong, err := client.Ping().Result()
	if err != nil {
		fmt.Println(pong, err)
		return nil
	}
	return client
}
