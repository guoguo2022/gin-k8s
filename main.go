package main

import (
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/conf"
	"gitee.com/cmlfxz/gin-k8s/src/model"
	"gitee.com/cmlfxz/gin-k8s/src/router"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func CreateDb() {
	var err error
	mysqlConf := conf.Setting.MySQLConfig
	host := mysqlConf.Host
	port := mysqlConf.Port
	database := mysqlConf.DataBase
	username := mysqlConf.UserName
	password := mysqlConf.PassWord

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, port, database)
	// dsn := "dev_user:abc123456@tcp(192.168.11.200:52100)/tutorial?charset=utf8mb4&parseTime=True&loc=Local"
	model.Db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("数据库连接失败")
		panic(err)
	}
	// defer db.Close()
	fmt.Println("数据库连接成功", &model.Db)
}

func main() {
	//加载配置文件
	conf.LoadConfig()
	CreateDb()
	r := router.InitRouter()
	r.Run(":8090")
}
